<?php
/**
 * PHP API usage example
 *
 * contributed by: Art of WiFi
 * description: example basic PHP script to disable/enable a device, returns true upon success
 */

/**
 * using the composer autoloader
 */
require_once('vendor/autoload.php');

/**
 * include the config file (place your credentials etc. there if not already present)
 * see the config.template.php file for an example
 */
require_once('config.php');


$parm = $argv[1];

//echo "PARM=".$parm."\n";
/**
 * the 24 character id of the device to disable/enable
 */
//$wlan_name = 'Unge';

/**
 * the site to which the device belongs
 */
// $site_id = ''
$true_false = false;
if($parm == 'true') {

//echo "TRUE_FALSE=".$parm."\n";
  $true_false = true;
}

/**
 * initialize the UniFi API connection class and log in to the controller
 */
$unifi_connection = new UniFi_API\Client($controlleruser, $controllerpassword, $controllerurl, $site_id, $controllerversion);
$set_debug_mode   = $unifi_connection->set_debug($debug);
$loginresults     = $unifi_connection->login();

$net_list = $unifi_connection->list_wlanconf();

//echo json_encode($net_list, JSON_PRETTY_PRINT);
$wlan_id='';
foreach ( $net_list as $wlan) {

	//echo $wlan->name.":".$wlan->_id."\n";
	if ($wlan->name == $wlan_name) {
		$wlan_id = $wlan->_id;
		break;	
	}
}
//echo "wlan_id=".$wlan_id."\n";

$wlan_response = $unifi_connection->list_wlanconf($wlan_id);
//print_r ($wlan_response['enabled']);
//print_r ($wlan_response[0]);

/*
 * then we disable the device
 */
$disable_result = $unifi_connection->disable_wlan($wlan_id, $true_false );
print_r ($disable_result);

/**
 * or we enable the device, uncomment as neccesary (then also comment the previous call)
 */
//$disable_result = $unifi_connection->disable_ap($wlan_id, false);

/**
 * provide feedback in json format
 */
//echo json_encode($disable_result, JSON_PRETTY_PRINT);
