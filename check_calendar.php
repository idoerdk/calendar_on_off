<?php
/**  
 * UniFi WiFi off calendar.
 * UniFi WiFi off calendar.Timezone management is dodgy at best
 * Does not manage diffent timezone events in the same feed
 * 
 * Check_calendar.php
 * 
 * @category Abc
 * @package  IDoerUtils
 * @author   "Jens Arnt" <jens@idoer.dk>
 * @license  http://idoer.dk MIT
 * @link     no-idea-what-this-does
 * 
 * UniFi WiFi off calendar.
 * Based on an icalendar feed (google calendar) a specific SSID will 
 * be turned off for the duration of calendar events
 * Logic is not overly complicated and does not manage overlapping events.
 *
 * TODO Fix timezone handling
 * TODO fix DST on/off
 * Versions
 * 
 * 1.9.3 -  exdates not taken into consideration when doing rrule
 * 1.9.2 -  Event spanning from thursday to friday morning 
 *          did not turn on the network friday morning.
 * 1.9.1 
 */

/**
 * Using the composer autoloader
 */
require_once 'vendor/autoload.php';

/**
 * Include the config file (place your credentials etc. there if not already present)
 * see the config.template.php file for an example
 */
require_once 'config.php';
require_once "config_calendar.php";

date_default_timezone_set("Europe/Copenhagen");
$local_tz=date_default_timezone_get();
//print "tz=".$local_tz."=\n"; 

$logfile=fopen($iCalLogfile, "a") or die("iCal log file can not be opened");

log_line("check_calendar starting");

$path_to_library = "icalendar/";
require_once $path_to_library . "/zapcallib.php";

$icalobj = null;
$eventcount = 0;
$maxevents = 500;
$start_stop_events=[];
$timezone = new DateTimeZone($local_tz);
//$datetime = new DateTime("now", $timezone);
$datetime = new DateTime("now");
$dateinterval = new DateInterval("PT1H"); // Outside DST
//$dateinterval = new DateInterval("PT2H"); // DST
$datetime->add($dateinterval);
$now = $datetime->getTimestamp();
//$now = ZDateHelper::addDate($now, 72,0,0,0,0,0);
//$now = time();

//$dateobj = new ZDateHelper();

//$now = ZDateHelper::now();
//$now = $now - 60*60*4;
$now_time = date('H:i', $now);
$maxdate = ZDateHelper::addDate($now, 0, 0, 0, 0, 9, 0);
//$maxdate = strtotime("+1W", $now);

print_r("now=".$now."(".date(DATE_RFC822, $now).")\n");
print_r("now+9=".$maxdate."(".date(DATE_RFC822, $maxdate).")\n");
print_r("time=".date("H:i", $now)."\n");

log_line("now=".$now."(".date(DATE_RFC822, $now)."/"."now+9=".$maxdate."(".date(DATE_RFC822, $maxdate)."/"."time=".date("H:i", $now));

if (in_array(date("H:i", $now), $iCalFetchTimes) or !file_exists($jsonFile)) {
    log_line("Reading iCal information from ".$iCalUrl);
    print("Reading iCal information from ".$iCalUrl."\n");
    read_iCal_file($iCalUrl);
    write_json_file($jsonFile);
} else {
    log_line("Reading iCal information from cached file at ".$jsonFile);
    print("Reading iCal information from cached file at ".$jsonFile."\n");
    $start_stop_events=read_json_file($jsonFile);
}
/*
* Now we have $start_stop_events as an array with elements date=unix datetime in GMT, action=on|off, summary=comment from original feed
*/
// make sure the events are sorted by date
usort($start_stop_events, "calendar_cmp");
// print_array($start_stop_events);
//print "\n\n";

perform_on_off();

write_lastrun_file($lastRunFile, $now);

log_line("check_calendar ending");
fclose($logfile);

function write_lastrun_file($filename, $data)
{
    file_put_contents($filename, $data);
}

function perform_on_off() 
{
    global $start_stop_events, $lastRunFile, $now;
    $events=get_current_event($start_stop_events, $now);
    //print "-\n";
    //print_array($events);
    //print "-\n";

    if (file_exists($lastRunFile)) {
        $last_run_timestamp=file_get_contents($lastRunFile);
        print "lastrunstamp=".$last_run_timestamp."\n";
    } else {
        $last_run_timestamp=0;
        print "lastrunstamp=".$last_run_timestamp."\n";
    }
    
    if (array_key_exists('date', $events['curr'])) { // Only do stuff if we have a valid 'curr' entry
        if ($events['curr']['date'] >$last_run_timestamp and $events['curr']['date'] <= $now ) {
            //print_r ("Turning network ".$events['curr']['action']." for event ".$events['curr']['summary']."\n");
            log_line("Turning network ".$events['curr']['action']." for event ".$events['curr']['summary']."\n");
            disable_wlan($events['curr']['action']=='off');
        }
    } else {
        print_r("No valid curr entry");
    }
}


function get_current_event($start_stop_events, $now, $lastrun=0)
{
    //print "inside get_current_event";
    $rc_events=['prev'=>[], 'curr'=>[], 'next'=>[]];
    foreach ($start_stop_events as $event) {
        $rc_events['prev'] = $rc_events['curr'];
        $rc_events['curr'] = $rc_events['next'];
        $rc_events['next']=$event;
        if ($event['date']>$now) {
            break;
        }
    }
    return $rc_events;
}

function calendar_cmp($a, $b)
{
    if ($a['date'] == $b['date']) {
        return 0;
    }
    return ($a['date'] < $b['date']) ? -1 : 1;
}

function print_array($events) 
{
    global $start_stop_events;
    foreach ($events as $key=>$ev) {
        // print_r ("Key = ".$key.":");
        log_line("Key = ".$key.":");
        foreach ($ev as $key2=>$element) {
            // print ", ".$key2." = ".$element;
            $extra = ($key2 == "date") ? " (".date(DATE_RFC822, $element).")":"";
            log_line(", ".$key2." = ".$element.$extra);
            //print "kkk";
        }
        // print("\n");
    }
}

function read_iCal_file($file_url)
{
    $icaldata = file_get_contents($file_url) or die("iCal file not found");
    $icalobj = new ZCiCal($icaldata);
    //print_r($icalobj);
    foreach ($icalobj->tree->child as $node) {
        if ($node->getName() == "VEVENT") {
            process_event($node);
        }
    }
}

function read_json_file($file_url)
{
    $json_string = file_get_contents($file_url) or die("json file not found");
    return json_decode($json_string, true); //true for array    
}

function write_json_file($filename)
{
    global $start_stop_events;
        //print_r ($start_stop_events);
    
    if (file_exists($filename)) {
        rename($filename, $filename.date("YmdHis"));
    }
    file_put_contents($filename, json_encode($start_stop_events));
}

function process_event($node) 
{
    global $dateobj,$maxdate,$now,$start_stop_events, $local_tz;
    $ts_date = [];
    $summary='';
    $dtstart='';
    $dtend='';
    $rrule='';
    $exdates = []; 
    print("Inside process_event\n");
    //print_r($node);
    //print "Processing NODE\n";
    foreach ($node->data as $key => $value) {
        //print("KEY=");
        //print_r ($key);
        //print("\n");
        switch ($key) {
        case "SUMMARY":
            $summary=$value->getValues();
            break;
        case "DTSTART":
            $dtstart=$value->getValues();
            //print("dtstart");
            //print($dtstart);
            //$exdates[$dtstart]=[];
            break;
        case "DTEND":
            $dtend=$value->getValues();
            break;
        case "RRULE":
            $rrule=$value->getValues();
            break;
        case "TRANSP":
            $transp=$value->getValues();
            break;
        case "EXDATE":
            print ("EXDATE\n");
            $exdatesiCal = explode(",", $value->getValues());
            print_r($exdatesiCal);
            foreach ($exdatesiCal as $dkey => $iCaldate) {
                //$exdates[$dtstart][$dkey] = fromiCalToUnixDateTime($iCaldate, $local_tz);
                $exdates[$dkey] = fromiCalToUnixDateTime($iCaldate, $local_tz);
                
            }
            print("X");
            print_r($exdates);
            print("Y");
            //print "EXDATESICAL";
            //print_r($exdatesiCal);
            //print "EXDATES";
            //print_r($exdates);
            break;
        }
    }
    if ($transp != "TRANSPARENT") {
        if ($rrule != "" ) {
            //print "RRULEXXX";
            //print_r($exdates);

            expand_date_rrule($rrule, fromiCalToUnixDateTime($dtstart), fromiCalToUnixDateTime($dtend), $maxdate, $exdates, "RRULE:".$summary."(".$dtstart.")");
            //expand_date_rrule($rrule,fromiCalToUnixDateTime($dtstart),$maxdate, 'off', "RRULE:".$summary."(".$dtstart.")");
            //expand_date_rrule($rrule,fromiCalToUnixDateTime($dtend),$maxdate, 'on', "RRULE:".$summary."(".$dtend.")");
        } else {
            //print "sumary=".$summary."\n";
            $start_stop_events[]=output_action(fromiCalToUnixDateTime($dtstart), $now, "off", "SINGLE:".$summary."(".$dtstart.")");
            $start_stop_events[]=output_action(fromiCalToUnixDateTime($dtend), $now, "on", "SINGLE:".$summary."(".$dtend.")");
            //print "\n\n";
        }
    }
}

/*
 * Not used ?????
 */
function getTimestampFromiCal($ical_datetime, $timezone) 
{
    print "ical_datetime=".$ical_datetime." ".$timezone;
    print "\n";

    $unixtimestamp = fromiCalToUnixDatetime($ical_datetime, $timezone);
    print "unixtimestamp=".$unixtimestamp;
    print "\n";
    $sqldt = ZDateHelper::toSqlDateTime($unixtimestamp);
    print "sqldt=".$sqldt;
    print "\n";
    $localunixts = ZDateHelper::toUnixDate($sqldt);
    print "localunixts=".$localunixts;
    print "\n";
    return $localunixts;
}

function expand_date_rrule($rrule,$from_date, $to_date, $max_date, $exdates, $summary) 
{
    global $local_tz, $now, $dateobj, $start_stop_events;
    print ("Inside expand_date_rrule");
    $event_duration=$to_date - $from_date;

    //$recurring_dates_obj= new ZCRecurringDate($rrule, $from_date, $exdates, $local_tz);
    //print_r ($exdates);
    $recurring_dates_obj= new ZCRecurringDate($rrule, $from_date, $exdates, $local_tz);
    //$recurring_dates_obj->setDebug(1);
    $dates = $recurring_dates_obj->getDates($max_date);
    foreach ($dates as $date) {
        
         //print_r($summary."/".$date."/".date(DATE_RFC822,$date)."\n");
        $start_stop_events[]=output_action($date, $now, 'off', $summary."/".$date."/".date(DATE_RFC822, $date));
        $start_stop_events[]=output_action($date+$event_duration, $now, 'on', $summary."/".$date."/".date(DATE_RFC822, $date+$event_duration));
    }
}


function XXXXexpand_date_rrule($rrule, $from_date, $max_date,  $action, $summary)
{
    global $local_tz, $now, $dateobj, $start_stop_events;
    $recurring_dates_obj= new ZCRecurringDate($rrule, $from_date, [], $local_tz);
    //$recurring_dates_obj->setDebug(1);
    $dates = $recurring_dates_obj->getDates($max_date);
    foreach($dates as $date) {
        //print_r($summary."/".$date."/".date(DATE_RFC822,$date)."\n");
        $start_stop_events[]=output_action($date, $now, $action, $summary);
    }
}

function fromiCaltoUnixDateTime($datetime, $timezone='UTC') 
{
    // first check format
    $formats = array();
    $formats[] = "/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]T[0-9][0-9][0-9][0-9][0-9][0-9]Z/";
    $formats[] = "/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]T[0-9][0-9][0-9][0-9][0-9][0-9]/";
    $formats[] = "/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/";
    $ok = false;
    foreach ($formats as $format) {
        if (preg_match($format, $datetime)) {
            $ok = true;
            //print "Found format:".$format;
            break;
        }
    }
    if (!$ok) {
        return null;
    }
    $year = substr($datetime, 0, 4);
    $month = substr($datetime, 4, 2);
    $day = substr($datetime, 6, 2);
    $hour = 0;
    $minute = 0;
    $second = 0;
    if (strlen($datetime) > 8 && $datetime{8} == "T") {
        $hour = substr($datetime, 9, 2);
        $minute = substr($datetime, 11, 2);
        $second = substr($datetime, 13, 2);
    }
    date_default_timezone_set($timezone);
    return mktime($hour, $minute, $second, $month, $day, $year);
}

function output_action($date, $now, $action, $summary)
{
    global $dateobj, $start_stop_events;
    // Will output to an arry 
    //$rc_array=[];
    $rc_array =['date' => $date, 'action'=>$action,'summary'=>$summary];
    // Will output to stdout
    // print $date.":".$action.":".$summary." (".$dateobj->fromUnixDateTimeToiCal($date).")\n";
    return $rc_array;
}

function log_line($text)
{
    global $logfile;
    date_default_timezone_set("Europe/Copenhagen");
    fwrite($logfile, date(DATE_W3C).":".$text."\n");
}


/**
 * PHP API usage example
 *
 * contributed by: Art of WiFi
 * description: example basic PHP script to disable/enable a device
 * returns true upon success
 */
function disable_wlan($disable = false)
{
    global $wlan_name, $site_id, $controlleruser, $controllerpassword, $controllerurl, $controllerversion,$dry_run, $debug;
    //    $debug=false;
    
    if (isset($dry_run) && $dry_run == true)  {
        print("Dry run!!! - \n");
        log_line("Dry run - diable=\n");
    
    } else {    
    // $wlan_name = 'Unge2';
    /**
     * the site to which the device belongs
     */
    // $site_id = ''
    /**
     * initialize the UniFi API connection class and log in to the controller
     */
    $unifi_connection = new UniFi_API\Client($controlleruser, $controllerpassword, $controllerurl, $site_id, $controllerversion);
    $set_debug_mode   = $unifi_connection->set_debug($debug);
    $loginresults     = $unifi_connection->login();
    print $loginresults;
    if ($loginresults != "") {
        $net_list = $unifi_connection->list_wlanconf();

        //echo json_encode($net_list, JSON_PRETTY_PRINT);
        $wlan_id='';
        //print_r ($net_list);
        foreach ( $net_list as $wlan) {
            echo $wlan->name.":".$wlan->_id."\n";
            if ($wlan->name == $wlan_name) {
                $wlan_id = $wlan->_id;
                break;    
            }
        }
        //echo "wlan_id=".$wlan_id."\n";

        /**
         * We disable the device
         */
        if (isset($dry_run) && $dry_run == true) {
            log_line("DRY RUN / disable_wlan(".$wlan_id.", ".$disable.")");
        } else {
            $disable_result = $unifi_connection->disable_wlan($wlan_id, $disable);
        }   
    } else {
        print("Logon did NOT succeed\n");
        log_line("Logon did NOT succeed");
    }
}
}
