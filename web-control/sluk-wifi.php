<?php
// Retrieve data from Query String
$setting = $_GET['wifi'];

require "on_off_calendar/wlan_control.php";

// Set helper variables
$ed="Ukendt";

function wlan_disable($enable_disable) {
	global $unifi_connection, $wlan_id;
	//echo $enable_disable;
	$disable_result = $unifi_connection->disable_wlan($wlan_id, $enable_disable );
	$rc=wlan_status();
	return $rc;
}

function wlan_status() {
	global $unifi_connection, $wlan_id;
	//echo $enable_disable;
	$wlan_response = $unifi_connection->list_wlanconf($wlan_id);
	$rc="Slukket";
	if ($wlan_response[0]->enabled == 1) {
		$rc="Tændt";
	}
	return $rc;
}

switch ($setting) {
	case "on": // Tænd ungenet
		$ed=wlan_disable(false);	
		break;
	case "off": // Sluk ungenet
		$ed=wlan_disable(true);	
		break;
	default:
		$ed=wlan_status();
		break;
}

//echo "<br />";
echo $ed;
//echo "<br />";
//echo "x".$servopos."x";
?>

